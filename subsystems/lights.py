from wpilib import Spark
import constants

class Lights:
    lightstrip: Spark

    #this guide can be consulted on numbers but testing may need to be done to confirm
    # https://www.revrobotics.com/content/docs/REV-11-1105-UM.pdf
    class Color:
        FLASH_GREEN = 0.15
        GREEN = 0.86
        YELLOW = 0.56 
        RED = 0.61 
        def __init__(self, name, value):
            self.name = name
            self.value = value

        def __str__(self):
            return self.name

    def setup(self):
        self.lightNumber = 0.0


    
    def setColor(self,number):
        self.lightNumber = number
        
    
    def execute(self):
        self.lightstrip.set(self.lightNumber)