import ntcore
import constants

class Limelight:
    #displaying values for crosshair offset on cameras
    def setup(self):
        self.LimelightTable = ntcore.NetworkTableInstance.getDefault().getTable("limelight")

    def foundValidTarget(self):
        return self.LimelightTable.getEntry("tv").getBoolean(False)
    
    def getHorizontalOffset(self):
        return self.LimelightTable.getEntry("tx").getDouble(0.0)
    
    def getVerticalOffset(self):
        return self.LimelightTable.getEntry("ty").getDouble(0.0)
    
    def getTargetArea(self):
        return self.LimelightTable.getEntry("ta").getDouble(0.0)
    
    def areWeCloseEnough(self):
        return (self.getTargetArea() >= constants.TARGET_PERCENT_OF_IMAGE)
    
    def hasTarget(self):
        return ((self.getHorizontalOffset() != 0.0) and (self.getVerticalOffset() != 0.0))

    def execute(self):
        pass
