import wpilib


class Table:
    class TableState:
        SCORE = True
        PICKUP = False
        def __init__(self, name, value):
            self.name = name
            self.value = value

        def __str__(self):
            return self.name
    class SliderState:
        IN = False
        OUT = True
        def __init__(self, name, value):
            self.name = name
            self.value = value

        def __str__(self):
            return self.name

    currentTableState: bool
    currentSliderState: bool
    tablePiston: wpilib.Solenoid
    sliderPiston: wpilib.Solenoid
    
    
    
    #set the state to pickup on startup
    def setup(self):
        self.currentTableState = self.TableState.PICKUP
        self.currentSliderState = self.SliderState.IN    
    
    ##TODO create new functions for Slider and rename the existing ones for the table as we may do different state changes for the mechanism or make this a "StateMachine" (see commands/PickupNote)
    
    #set current state to new state
    def updateSliderState(self,newState: SliderState):
        self.currentSliderState = newState
    
    #toggle the state between pickup and score
    def toggleSliderState(self):
        self.currentSliderState = not self.currentSliderState

    #set current state to new state
    def updateTableState(self,newState: TableState):
        self.currentTableState = newState

    #toggle the state between pickup and score
    def toggleTableState(self):
        self.currentTableState = not self.currentTableState

    def execute(self):
        self.tablePiston.set(self.currentTableState)
        self.sliderPiston.set(self.currentSliderState)

