import wpilib
from rev import CANSparkMax
import constants

class Climber:
    climbMotor: CANSparkMax
    climbSpeed: float = 0.0

    def setup(self):
        self.climbMotor.restoreFactoryDefaults()
        self.climbMotor.setSmartCurrentLimit(constants.PEAK_MOTOR_AMPS)
        self.climbMotor.setIdleMode(CANSparkMax.IdleMode.kBrake)


    def setMotorSpeed(self, climbSpeed):
        self.climbSpeed = climbSpeed

    def execute(self):
        self.climbMotor.set(self.climbSpeed)

