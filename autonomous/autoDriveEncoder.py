from magicbot import AutonomousStateMachine, timed_state, state
from rev import CANSparkMax
import wpilib
import constants
from wpimath.controller import PIDController



# this is one of your components
from subsystems.drivetrain import Drivetrain


class DriveForwardEncoder(AutonomousStateMachine):

    MODE_NAME = "Drive Forward Encoder"
    DEFAULT = True

    # Injected from the definition in robot.py
    drivetrain: Drivetrain

    target_position = constants.DRIVE_ENCODER_TICKS_FWD_DISTANCE

    @state(first=True)
    def drive_forward_distance(self):

        self.leftPostion = self.drivetrain.leftEncoder.getPosition()
        self.rightPosition = self.drivetrain.rightEncoder.getPosition()

        self.averagePositon = (self.leftPostion + self.rightPosition) /2.0

        #PID VALUES Porportional,Intergral,Derivitive
        P = constants.DRIVE_PID_P; #How fast we move towards the target
        offset = self.target_position - self.averagePositon
        speed = offset * P

        if speed > constants.DRIVE_PID_KMAX: #the speed we want is more than the motor can handle (1.0)
            speed = constants.DRIVE_PID_KMAX
        elif speed < constants.DRIVE_PID_KMIN: #the speed we want is less than the motor can handle (-1.0)
            speed = constants.DRIVE_PID_KMIN

        self.drivetrain.setMotorSpeeds(speed,0)
        
        if offset < constants.DRIVE_ENCODER_DEADBAND: # we dont need to be "0.0" for the offset just close enough
            self.next_state("stop")


        wpilib.SmartDashboard.putNumber("Target Position", constants.DRIVE_ENCODER_TICKS_FWD_DISTANCE)
        wpilib.SmartDashboard.putNumber("encoderPostionL",self.drivetrain.leftEncoder.getPosition())
        wpilib.SmartDashboard.putNumber("encoderPostionR",self.drivetrain.rightEncoder.getPosition())
    
    @state()
    def stop(self):
        #using the encoders to drive a set distance using a PID
        self.drivetrain.setMotorSpeeds(0,0)
        