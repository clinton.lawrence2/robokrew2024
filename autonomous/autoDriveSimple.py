from magicbot import AutonomousStateMachine, timed_state, state
from rev import CANSparkMax
import constants

# this is one of your components
from subsystems.drivetrain import Drivetrain


class DriveForwardEncoder(AutonomousStateMachine):

    MODE_NAME = "Drive Forward Simple"
    DEFAULT = False

    # Injected from the definition in robot.py
    drivetrain: Drivetrain

    @timed_state(duration=3, first=True,next_state="stop")
    def drive_forward_distance(self):
        self.drivetrain.setMotorSpeeds(0.75,0.0)
    
    @state()
    def stop(self):
        self.drivetrain.setMotorSpeeds(0,0)
        