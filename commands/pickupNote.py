import wpilib
from magicbot import StateMachine, state, timed_state
# from subsystems.lights import Lights
from subsystems.table import Table

class PickupNote(StateMachine):
    table: Table
    # lights: Lights

    def pickupNote(self):
        self.engage()
    
    @state(first=True)
    def tablePickup(self):
        self.table.updateTableState(Table.TableState.PICKUP)
        wpilib.SmartDashboard.putString("TABLE STATE", "PICKUP")
        self.next_state("retract")
    
    @state()
    def retract(self):
        #extend slider
        self.table.updateSliderState(Table.SliderState.IN)
        #goto next state
        # self.next_state("flashLights")
    
    # @state()
    # def flashLights(self):
        # self.lights.setColor(Lights.Color.FLASH_GREEN)



    
