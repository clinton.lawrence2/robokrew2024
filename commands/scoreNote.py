import wpilib
from magicbot import StateMachine, state, timed_state
from subsystems.table import Table

class ScoreNote(StateMachine):
    table: Table

    # def scoreNote(self):
    #     self.engage()
    
    @timed_state(duration=1.0,first=True,must_finish=True,next_state="extendSlider")
    def tableScore(self):
        wpilib.SmartDashboard.putString("CurrentScoreState", self.current_state)
        self.table.updateTableState(Table.TableState.SCORE)
    
    @timed_state(duration=1.0,next_state="retract",must_finish=True)
    def extendSlider(self):
        print("INSIDE EXTEND SLIDER")
        #extend slider
        wpilib.SmartDashboard.putString("CurrentScoreState", self.current_state)
        self.table.updateTableState(Table.TableState.SCORE)
        self.table.updateSliderState(Table.SliderState.OUT)
    
    @timed_state(duration=1.0,must_finish=True,next_state="tablePickup")
    def retract(self):
        print("INSIDE RETRACT STATE")
        #retract slider
        wpilib.SmartDashboard.putString("CurrentScoreState", self.current_state)
        self.table.updateSliderState(Table.SliderState.IN)
        
    @timed_state(duration=1.0,must_finish=True, next_state="extendSliderAgain")
    def tablePickup(self):
        #restore table back to pickup state
        print("INSIDE TABLE PICKUP STATE")
        wpilib.SmartDashboard.putString("CurrentScoreState", self.current_state)
        self.table.updateTableState(Table.TableState.PICKUP)
    
    @timed_state(duration=1.0,must_finish=True,next_state="retractAgain")
    def extendSliderAgain(self):
        print("INSIDE EXTEND SLIDER")
        #extend slider
        self.table.updateSliderState(Table.SliderState.OUT)

    @state(must_finish=True)
    def retractAgain(self):
        print("INSIDE RETRACT STATE")
        #retract slider
        self.table.updateSliderState(Table.SliderState.IN)
        self.done()





    
